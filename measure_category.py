from enum import IntEnum

class MeasureCategory(IntEnum):
    Real = 1
    UserObjective = 2