import time
import datetime, calendar
import pprint # Pour affichage de json un peu plus lisible
from withings_service import WithingsService
from measure_type import MeasureType
from measure_category import MeasureCategory

if __name__ == '__main__':
    client_id = ''
    client_secret = ''
    redirect_uri = 'https://www.withings.com'

    service = WithingsService(client_id, client_secret, redirect_uri)
    service.get_authentication_code('user.info,user.metrics,user.activity,user.sleepevents')
    # Récupérer la valeur du paramètre 'code' dans l'URL qui est ouverte
    # exemple: https://www.withings.com/fr/fr/?code=f64e3c1ffac0143e9cd4c68e335ab20919a06981&state=biowithingsapp_state
    # la valeur du code est 'f64e3c1ffac0143e9cd4c68e335ab20919a06981'
    authentication_code = input('Enter the authentication code you got: ')

    # L'access token a une durée de vie de 3h. Vous n'avez plus besoin de faire les étapes précédentes pour 3h
    tokens = service.get_access_token(authentication_code)
    access_token = tokens['access_token']
    # access_token = '828be8ffd3f2f6cea0318c6ff0a5081d7d2e15e9'
    print('getting access token succeeded')
    print("access_token: %s, refresh_token: %s" % (access_token, tokens['refresh_token']))

    devices = service.get_device(access_token)
    print('user devices:')
    pprint.pprint(devices)

    # Calculate a start date that is 3 days ago
    end_date = datetime.datetime.utcnow()
    delta = datetime.timedelta(days = 3)
    start_date = end_date - delta
    # Convert datetime to unix
    end_date = calendar.timegm(end_date.timetuple())
    start_date = calendar.timegm(start_date.timetuple())

    measures = service.get_measures(access_token, MeasureType.Weight, MeasureCategory.Real, start_date, end_date)
    print('user measures:')
    pprint.pprint(measures)

    activities = service.get_activities(access_token, '2019-10-10', '2019-10-23')
    print('user activities:')
    pprint.pprint(activities)