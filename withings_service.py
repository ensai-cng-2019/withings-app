import requests
import webbrowser

class WithingsService:
    """
    Represents a Withings service class to authenticate and request data from Withings API.

    http://developer.withings.com/oauth2
    """

    def __init__(self, client_id, client_secret, redirect_uri, enable_proxies = False):
        """
        Constructor

        :param client_id: client id OAuth
        :param client_secret: client secret OAuth
        :param redirect_uri: redirection uri for authentication code
        :param enable_proxies: enable proxies at Ensai
        :type client_id: string
        :type client_secret: string
        :type redirect_uri: string
        :type enable_proxies: string
        :return: A Withings service
        :rtype: WithingsService
        """
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri
        if enable_proxies:
            self.proxies = {
                'http': 'http://pxcache-02.ensai.fr:3128',
                'https': 'http://pxcache-02.ensai.fr:3128'
            }
        else:
            self.proxies = { }

    def get_authentication_code(self, scope):
        """
        Get authentication code. This method will a web browser and redirect to an authorization page. 
        User must sign with its Withings account and autorize the partner application.
        http://developer.withings.com/oauth2/#tag/OAuth-2.0%2Fpaths%2Fhttps%3A~1~1account.withings.com~1oauth2_user~1authorize2%3Fresponse_type%3Dcode%5B...%5D%2Fget
        """
        url = 'https://account.withings.com/oauth2_user/authorize2'

        params = {
            'response_type': 'code',
            'client_id': self.client_id,
            'state': 'biowithingsapp_state',
            'scope': scope,
            'redirect_uri': self.redirect_uri
        }

        # Comme décrit dans la documentation d'API (http://developer.withings.com/oauth2/#tag/OAuth-2.0%2Fpaths%2Fhttps%3A~1~1account.withings.com~1oauth2_user~1authorize2%3Fresponse_type%3Dcode%5B...%5D%2Fget)
        # La méthode HTTP est de type 'GET' et les paramètres sont à mettre dans le chemin d'URL (utiliser le paramètre params comme ci-dessous)
        response = requests.get(
            url,
            proxies=self.proxies,
            params=params
        )
        print('response.url')
        print(response.url)

        # Ouverture du web browser pour récupérer le authentication code
        webbrowser.open(response.url)

    def get_access_token(self, authentication_code):
        """
        Get access token. This method will get the access token and refresh token with the authentication code provided.
        http://developer.withings.com/oauth2/#tag/OAuth-2.0%2Fpaths%2Fhttps%3A~1~1account.withings.com~1oauth2~1token%20%5Bgrant_type%3Dauthorization_code...%5D%2Fpost
        """
        url = 'https://account.withings.com/oauth2/token'

        body = {
            'grant_type': 'authorization_code',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': authentication_code,
            'redirect_uri': self.redirect_uri
        }

        # Comme décrit dans la documentation d'API
        # La méthode HTTP est de type 'POST' et les paramètres sont à mettre dans le corps de la requête cette fois (paramètre 'data' ci-dessous)
        response = requests.post(
            url,
            proxies=self.proxies,
            data=body
        )
        print('response')
        print(response.json())

        return response.json()

    def get_device(self, access_token):
        """
        http://developer.withings.com/oauth2/#tag/user%2Fpaths%2Fhttps%3A~1~1wbsapi.withings.net~1v2~1user%3Faction%3Dgetdevice%2Fget
        """
        url = 'https://wbsapi.withings.net/v2/user'

        # Pour authentifier votre appel à l'API, il faut maintenant ajouter l'access_token dans les headers de votre requête REST
        headers = {
            'Authorization': 'Bearer {}'.format(access_token),
            'Content-Type': 'application/json',
        }
        # Il faut ajouter le paramètre 'action' qui a la valeur 'getdevice' dans le chemin de l'URL
        params = {
            'action': 'getdevice'
        }

        response = requests.get(
            url,
            headers=headers,
            proxies=self.proxies,
            params=params
        )
        return response.json()

    def get_measures(self, access_token, measure_type, measure_category, start_date, end_date):
        """
        Return health measures for a specified date range.

        This will return a dictionary that contains health measures.

        :param access_token: client id OAuth
        :param measure_type: client secret OAuth
        :param measure_category: measure category
        :param start_date: start date
        :param end_date: end date
        :type access_token: string
        :type measure_type: string
        :type measure_category: string
        :type start_date: string
        :param end_date: string
        :return: Health measures
        :rtype: dictionary
        
        Doc d'api
        http://developer.withings.com/oauth2/#tag/measure%2Fpaths%2Fhttps%3A~1~1wbsapi.withings.net~1measure%3Faction%3Dgetmeas%2Fget
        """
        url = 'https://wbsapi.withings.net/measure'

        # Pour authentifier votre appel à l'API, il faut maintenant ajouter l'access_token dans les headers de votre requête REST
        headers = {
            'Authorization': 'Bearer {}'.format(access_token),
            'Content-Type': 'application/json',
        }
        # Il faut ajouter le paramètre 'action' qui a la valeur 'getmeas' dans le chemin de l'URL
        params = {
            'action': 'getmeas',
            'meastype': measure_type,
            'category': measure_category,
            'startdate': start_date,
            'enddate': end_date
        }
        print('start_date: %s, end_date: %s' % (start_date, end_date))

        response = requests.get(
            url,
            headers=headers,
            proxies=self.proxies,
            params=params
        )
        return response.json()

    def get_activities(self, access_token, start_date, end_date):
        """
        http://developer.withings.com/oauth2/#tag/user%2Fpaths%2Fhttps%3A~1~1wbsapi.withings.net~1v2~1user%3Faction%3Dgetdevice%2Fget
        """
        url = 'https://wbsapi.withings.net/v2/measure'

        # Pour authentifier votre appel à l'API, il faut maintenant ajouter l'access_token dans les headers de votre requête REST
        headers = {
            'Authorization': 'Bearer {}'.format(access_token),
            'Content-Type': 'application/json',
        }
        # Il faut ajouter le paramètre 'action' qui a la valeur 'getmeas' dans le chemin de l'URL
        params = {
            'action': 'getactivity',
            'startdateymd': start_date,
            'enddateymd': end_date,
            'data_fields': 'steps,distance'
        }
        print('start_date: %s, end_date: %s' % (start_date, end_date))

        response = requests.get(
            url,
            headers=headers,
            proxies=self.proxies,
            params=params
        )
        return response.json()
