from enum import IntEnum

class MeasureType(IntEnum):
    Weight = 1
    Height = 4
    HeartRate = 11